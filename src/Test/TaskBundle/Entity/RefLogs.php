<?php

namespace Test\TaskBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="ref_logs")
 */
class RefLogs
{
    /**
     * @var integer 
     *
     * @ORM\Column(name="ref", type="integer")
     * @ORM\Id
     */
    protected $ref;
	
	/**
     * @ORM\Column(type="integer")
     */
    protected $ip;
	
	/**
     * @ORM\Column(type="string", length=255)
     */
    protected $referer;
	
	/**
     * @ORM\Column(type="integer")
     */
    protected $date;
	
	public function setRef($v)
	{
		$this->ref = $v;
	}
	
	public function setIp($v)
	{
		$this->ip = $v;
	}
	
	public function setReferer($v)
	{
		$this->referer = $v;
	}
	
	public function setDate($v)
	{
		$this->date = $v;
	}
	
}