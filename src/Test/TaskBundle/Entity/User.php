<?php

namespace Test\TaskBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
	
    protected $ref_code;

    public function __construct()
    {
        parent::__construct();
        // your own logic
		$this->ref_code = substr(md5(uniqid(rand(), true)), 0, 6);
    }
}