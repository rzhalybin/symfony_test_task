<?php

namespace Test\TaskBundle\EventListener;

use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Test\TaskBundle\Entity\User;
use Test\TaskBundle\Entity\RefLogs;

class RefListener
{
	protected $session;
	
    function __construct($session)
    {
        $this->session = $session;
    }
	
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        if (HttpKernel::MASTER_REQUEST == $event->getRequestType() && $request->query->has('ref')) {
			$this->session->set('ref', $request->query->get('ref'));
			$this->session->set('ref_url', $request->server->get('HTTP_REFERER'));
		}
    }
	
	public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
		$em = $args->getEntityManager();
		file_put_contents('D:\2.txt', print_r($entity, true));
        if ($entity instanceof User) {
			file_put_contents('D:\2.txt', print_r($this->session, true));
            if ($this->session->has('ref')) {
				$user = $em
					->getRepository('TestTaskBundle:User')
					->findOneBy(array('ref_code' => $this->session->get('ref')));
				file_put_contents('D:\2.txt', print_r($user, true));
				$uid = $user->getId();
				file_put_contents('D:\2.txt', print_r($uid, true));
				if ($user && $user->getId()) {
					$log = new RefLogs();
					
					$ip = ip2long($_SERVER['REMOTE_ADDR']);
					$referer = $this->session->get('ref_url');
					$log->setRef($user->getId());
					$log->setIp($ip ? $ip : 0 );
					$log->setReferer($referer ? $referer : '');
					$log->setDate(time());
					
					$em->persist($log);
					$em->flush();
				}
            }
        }
    }
}